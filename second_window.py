import arcade


class MySecondWindow(arcade.Window):
    def __init__(self, width: int = 800, height: int = 600, title = "My first game"):
        super().__init__(width, height, title)
        self.set_location(0, 0)
        arcade.set_background_color(arcade.color.COOL_BLACK)
        
        self._player_x = 100
        self._player_y = 200
        self._player_speed = 250
        
        self._spaceship = arcade.Sprite("sprites/spaceship.png", scale=0.2, center_x=100, center_y= 150)
        
        self._right = False
        self._left = False
        self._up = False
        self._down = False
        
        
    def on_draw(self):
        arcade.start_render()
        self._spaceship.draw()
        return super().on_draw()

    def on_update(self, delta_time: float):
        print(delta_time)
        if self._right:
            self._player_x += self._player_speed * delta_time
            self._spaceship.turn_right(2)
        if self._left:
            self._player_x -= self._player_speed * delta_time
            self._spaceship.turn_left(2)
        if self._up:
            self._player_y += self._player_speed * delta_time
            self._spaceship.strafe(2)
        if self._down:
            self._player_y -= self._player_speed * delta_time
        self._spaceship.set_position(self._player_x, self._player_y)
        return super().on_update(delta_time)
    
    def on_key_press(self, symbol: int, modifiers: int):
        if symbol == arcade.key.RIGHT:
            self._right = True
        if symbol == arcade.key.LEFT:
            self._left = True
        if symbol == arcade.key.UP:
            self._up = True
        if symbol == arcade.key.DOWN:
            self._down = True
        return super().on_key_press(symbol, modifiers)
    
    def on_key_release(self, symbol: int, modifiers: int):
        if symbol == arcade.key.RIGHT:
            self._right = False
        if symbol == arcade.key.LEFT:
            self._left = False
        if symbol == arcade.key.UP:
            self._up = False
        if symbol == arcade.key.DOWN:
            self._down = False
        return super().on_key_release(symbol, modifiers)
    
    def on_mouse_press(self, x: int, y: int, button: int, modifiers: int):
        if button == arcade.MOUSE_BUTTON_LEFT:
            if x < self._player_x:
                self._left = True
            elif x > self._player_x:
                self._right = True
        if button == arcade.MOUSE_BUTTON_RIGHT:
            pass
        return super().on_mouse_press(x, y, button, modifiers) 
    
    def on_mouse_release(self, x: int, y: int, button: int, modifiers: int):
        self._right = False
        self._left = False
        self._up = False
        self._down = False
        return super().on_mouse_release(x, y, button, modifiers)
    
    def on_mouse_motion(self, x: int, y: int, dx: int, dy: int):
        return super().on_mouse_motion(x, y, dx, dy)
    
    