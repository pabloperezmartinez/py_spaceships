import arcade


class MyFirstWindow(arcade.Window):
    def __init__(self, width: int = 800, height: int = 600, title = "My first game"):
        super().__init__(width, height, title)
        self.set_location(0, 0)
        arcade.set_background_color(arcade.color.TEAL_BLUE)
        self._c_pos_x = 100
        self._c_pos_y = 100
        self._c2_pos_x = 200
        self._c2_pos_y = 200
        self._c3_pos_x = 300
        self._c3_pos_y = 300
        self._radius = 50
        self._c_pos_x_speed = 150
        self._c_pos_y_speed = 250
        self._player_x = 100
        self._player_y = 100
        self._player_speed = 250
        
        self._right = False
        self._left = False
        self._up = False
        self._down = False
        
        
    def on_draw(self):
        arcade.start_render()
        arcade.draw_line(20, 200, 310, 230, arcade.color.WHITE_SMOKE, 4)
        arcade.draw_point(30, 240, arcade.color.AMARANTH_PINK, 10)
        arcade.draw_circle_filled(self._c_pos_x, self._c_pos_y, self._radius, arcade.color.GO_GREEN, 50, 50)
        arcade.draw_circle_filled(self._c2_pos_x, self._c2_pos_y, self._radius, arcade.color.YELLOW, 50, 50)
        arcade.draw_circle_filled(self._c3_pos_x, self._c3_pos_y, self._radius, arcade.color.RED_DEVIL, 50, 50)
        arcade.draw_circle_outline(self._player_x, self._player_y, 50, arcade.color.AFRICAN_VIOLET, 2, 50, 50)
        arcade.draw_lrtb_rectangle_filled(10, 40, 500, 300, arcade.color.BABY_BLUE)
        return super().on_draw()

    def on_update(self, delta_time: float):
        self._c_pos_x += self._c_pos_x_speed * delta_time
        self._c_pos_y += self._c_pos_y_speed * delta_time
        if self._c_pos_x >= (self.width - self._radius) or self._c_pos_x <= self._radius:
            self._c_pos_x_speed *= -1
        if self._c_pos_y >= self.height - self._radius or self._c_pos_y <= self._radius:
            self._c_pos_y_speed *= -1
            
        if self._right:
            self._player_x += self._player_speed * delta_time
        if self._left:
            self._player_x -= self._player_speed * delta_time
        if self._up:
            self._player_y += self._player_speed * delta_time
        if self._down:
            self._player_y -= self._player_speed * delta_time
        print(delta_time)
        return super().on_update(delta_time)
    
    def on_key_press(self, symbol: int, modifiers: int):
        if symbol == arcade.key.RIGHT:
            self._right = True
        if symbol == arcade.key.LEFT:
            self._left = True
        if symbol == arcade.key.UP:
            self._up = True
        if symbol == arcade.key.DOWN:
            self._down = True
        return super().on_key_press(symbol, modifiers)
    
    def on_key_release(self, symbol: int, modifiers: int):
        if symbol == arcade.key.RIGHT:
            self._right = False
        if symbol == arcade.key.LEFT:
            self._left = False
        if symbol == arcade.key.UP:
            self._up = False
        if symbol == arcade.key.DOWN:
            self._down = False
        return super().on_key_release(symbol, modifiers)
    
    def on_mouse_press(self, x: int, y: int, button: int, modifiers: int):
        if button == arcade.MOUSE_BUTTON_LEFT:
            self._c_pos_x = x
            self._c_pos_y = y
        if button == arcade.MOUSE_BUTTON_RIGHT:
            self._c2_pos_x = x
            self._c2_pos_y = y
        return super().on_mouse_press(x, y, button, modifiers) 
    
    def on_mouse_motion(self, x: int, y: int, dx: int, dy: int):
        self._c3_pos_x = x
        self._c3_pos_y = y
        return super().on_mouse_motion(x, y, dx, dy)